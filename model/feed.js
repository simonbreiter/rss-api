let mongoose = require('mongoose')
let Schema = mongoose.Schema

let FeedSchema = Schema({
    name: {type: String, required: true},
    url: {type: String, required: true}
})

module.exports = mongoose.model('Feed', FeedSchema)
