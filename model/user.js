let mongoose = require('mongoose')
let Schema = mongoose.Schema

let UserSchema = Schema({
    name: {type: String, required: true},
    password: {type: String, required: true},
    admin: Boolean,
    feeds: Array
})

module.exports = mongoose.model('User', UserSchema)
