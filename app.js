let mongoose = require('mongoose')
let express = require('express')
let app = express()
let morgan = require('morgan')
const config = require('./config')
let db = mongoose.connection
const port = process.env.PORT || 3000
let bodyParser = require('body-parser')

// Setup and connect to database
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
    console.log("Successfully connected to Database!")
})
mongoose.connect('mongodb://localhost/feeds') // Connect to database

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// Use morgan to log requests to the console
app.use(morgan('dev'))

app.use(require('./controllers'))

app.listen(port, function () {
    console.log('App listen on Port ' + port)
})
