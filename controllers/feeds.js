let mongoose = require('mongoose')
let express = require('express')
let router = express.Router()
let Feed = require('../model/feed')
let User = require('../model/user')
let app = express()
let bodyParser = require('body-parser')

// Use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

router.route('/users/:user_id/feeds')
// route that create a feed for the user with id (accessed at POST http://localhost:3000/api/users/{user_id}/feeds)
    .post(function (req, res) {
        let feed = new Feed()
        feed.name = req.body.name
        feed.url = req.body.url

        // Add feed to user
        User.findById(req.params.user_id, function (err, user) {

            user.feeds.push(feed._id)

            user.save(function (err) {
                if (err) {
                    res.send(err)
                }
            })
        })

        // Save new feed
        feed.save(function (err) {
            if (err) {
                res.send(err)
            }
            res.json({message: 'Feed created!'})
        })
    })
    // route that get all the feeds of user with this id (accessed at GET http://localhost:3000/api/users/{user_id}/feeds)
    .get(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }
            Feed.find({"_id": {$in: user.feeds}}, function (err, feeds) {
                if (err) {
                    res.send(err)
                }
                res.json(feeds)
            })
        })

    })

router.route('/users/:user_id/feeds/:feed_id')
// route that get the feed with that id of user with that id (accessed at GET http://localhost:3000/api/users/{user_id}/feeds/{feed_id})
    .get(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }
            if(user.feeds.includes(req.params.feed_id)) {
                Feed.findById(req.params.feed_id, function (err, feeds) {
                    if (err) {
                        res.send(err)
                    }
                    res.json(feeds)
                })
            }
            else {
                res.json({message: 'This user has no feed with this ID.'})
            }
        })
    })
    // route that update the feed with this id of user with that id (accessed at PUT http://localhost:3000/api/users/{user_id}/feeds/{feed_id})
    .put(function (req, res) {

        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }

            if(user.feeds.includes(req.params.feed_id)) {
                // use our feed model to find the feed we want
                Feed.findById(req.params.feed_id, function (err, feed) {

                    if (err) {
                        res.send(err)
                    }

                    if (req.body.name) {
                        feed.name = req.body.name  // update the feed name
                    }

                    if (req.body.url) {
                        feed.url = req.body.url  // update the feed url
                    }

                    // save the feed
                    feed.save(function (err) {
                        if (err) {
                            res.send(err)
                        }
                        res.json({message: 'Feed updated!'})
                    })

                })
            }
            else {
                res.json({message: 'This user has no feed with this ID.'})
            }
        })

    })
    // route that delete the feed with this id of user with this id (accessed at DELETE http://localhost:3000/api/feeds/:name)
    .delete(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }
            if(user.feeds.includes(req.params.feed_id)) {
                // remove feed from user
                User.update(
                    {
                        _id : req.params.user_id
                    }, {
                        $pull: {
                            'feeds': req.params.feed_id
                        }
                    }, function(err, user) {
                        if (err) {
                            res.send(err)
                        }
                        // remove document
                        Feed.remove({
                            _id : req.params.feed_id
                        }, function (err, feed) {
                            if (err) {
                                res.send(err)
                            }
                            res.json({message: 'Successfully deleted'})
                        })
                    })
            }
            else {
                res.json({message: 'This user has no feed with this ID.'})
            }
        })
    })

module.exports = router
