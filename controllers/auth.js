let mongoose = require('mongoose')
let express = require('express')
let app = express()
let router = express.Router()
let User = require('../model/user')
let bodyParser = require('body-parser')
let jwt = require('jsonwebtoken')
const config = require('../config')

app.set('secretSauce', config.secret) // Secret variable

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// route to authenticate a user (POST http://localhost:8080/api/authenticate)
router.route('/authenticate')
    .post(function (req, res) {
        // find the user
        User.findOne({
            name: req.body.name
        }, function (err, user) {

            if (err) {
                throw err
            }

            // if the user not exists, return failure
            if (!user) {
                res.json({success: false, message: 'Authentication failed. User not found.'})
            }

            else if (user) {

                // check if password matches, if not return failure
                if (user.password != req.body.password) {
                    res.json({success: false, message: 'Authentication failed. Wrong password.'})
                }

                else {

                    // if user is found and password is right
                    // create a token
                    var token = jwt.sign(user, app.get('secretSauce'), {
                        expiresIn: '1440m' // expires in 24 hours
                    })

                    // return the information including token as JSON
                    res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token
                    })
                }
            }
        })
    })

module.exports = router
