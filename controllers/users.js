let mongoose = require('mongoose')
let express = require('express')
let app = express()
let router = express.Router()
let User = require('../model/user')
let bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

router.route('/users')
    // route to create a new user (POST http://localhost:3000/api/users)
    .post(function (req, res) {
        let user = new User()
        user.name = req.body.name
        user.password = req.body.password

        user.save(function(err) {
            if (err) {
                res.send(err)
            }
            res.json({message: 'User created!'})
        })
    })

router.use(require('../middlewares/auth'))

router.route('/users')
    // route to return all users (GET http://localhost:3000/api/users)
    .get(function (req, res) {
        User.find({}, function (err, users) {
            res.json(users)
        })
    })

router.route('/users/:user_id')
    .get(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            res.json(user)
        })
    })
    .put(function(req, res){
        User.findById(req.params.user_id, function(err, user) {
            if (err) { res.send(err) }
            if(req.body.name) {
                user.name = req.body.name
            }
            if(req.body.password) {
                user.password = req.body.password
            }
            console.log(user)
            user.save(function (err) {
                if (err) { res.send(err) }
                res.json({message: 'User updated!'})
            })

        })

    })
    .delete(function(req, res){
        User.remove({_id : req.params.user_id}, function(err, user) {
            if (err) { res.send(err) }
            res.json({message: 'User successfully deleted'})
        })
    })

module.exports = router
