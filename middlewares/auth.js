let express = require('express')
let jwt = require('jsonwebtoken')
let app = express()
const config = require('../config')
let bodyParser = require('body-parser')

app.set('secretSauce', config.secret) // Secret variable

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

module.exports = function(req, res, next) {

    var token = req.body.token || req.query.token || req.headers['x-access-token']

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, app.get('secretSauce'), function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' })
            }
            else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded
                next()
            }
        })
    }
    else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        })
    }
}
